USE company;

# WEEK 8
# 1.
SELECT ShipperID, OrderID 
FROM orders
WHERE ShipperID IN (
	SELECT ShipperID FROM shippers
);


# 2.
SELECT shipperid, COUNT(OrderId) AS 'No. of orders'
FROM orders
WHERE ShipperID IN (
	SELECT ShipperID FROM shippers
)
GROUP BY ShipperID;

# OR
SELECT ShipperName, COUNT(OrderId) AS 'No. of orders'
FROM orders JOIN shippers ON orders.ShipperID = shippers.ShipperID GROUP BY orders.ShipperID;


# 3. Find customer names who have made at least one order at our company
SELECT CustomerName, OrderID 
FROM orders
JOIN customers on customers.CustomerID = orders.CustomerID
ORDER BY OrderID;

# 3a. All customer names, whether or not they made any orders
SELECT CustomerName, OrderID 
FROM customers
LEFT JOIN orders on customers.CustomerID = orders.CustomerID
ORDER BY OrderID;


# 4 Find all the employee names that prepared an order
SELECT FirstName, LastName, OrderID
FROM orders 
JOIN employees on orders.EmployeeID = employees.EmployeeID
ORDER BY OrderID;


# 4a Find all the employee names that prepared an order 
# and list the order ids that they worked on if they prepared any orders
SELECT FirstName, LastName, OrderID
FROM orders 
RIGHT JOIN employees on orders.EmployeeID = employees.EmployeeID
ORDER BY OrderID;

# WEEK 9
# 1. Replace "weird" spaces in cities and countries' names (For example change "Fra nce" to "France")
# You need to disable safe mode in MySQL Workbench settings and restart the MySQL server
# OR add the line below:
SET SQL_SAFE_UPDATES = 0;

UPDATE customers 
SET country = REPLACE(country, '\n', '');

UPDATE customers 
SET country = REPLACE(city, '\n', '');


# 2. Create a view for displaying information (customerID, name and contact name) about customers living in Mexico
CREATE VIEW mexicanCustomers AS
SELECT customerID, customerName, contactName
FROM customers WHERE country = 'México D.F.';

# Test the view
SELECT * FROM mexicanCustomers;


# 3. Find all orders made by the Mexican customers
SELECT * FROM mexicanCustomers
JOIN orders ON orders.customerID = mexicanCustomers.customerID;


# 4. Create a view that stores ID, name and price of products whose price is lower than the average price of the entire product catalog
CREATE VIEW productsBelowAvg AS
SELECT productID, productName, price
FROM products
WHERE price < ( SELECT AVG(price) FROM products );

# Test the view
SELECT * FROM productsBelowAvg;


# 5. Delete rows with product ID 5 
# (do not actually run this command)
# DELETE FROM orderDetails WHERE productID = 5;


# 6. Truncate - removes the content of table orderdetails 
# (similar to delete but there is no way to use "WHERE" and TRUNCATE is faster)
# (do not actually run this command)
# TRUNCATE orderdetails;


# 7. This will not work beacuse customerID is used in table "orders" as a foreign key
DELETE FROM customers;

# 8. Remove the whole table
# (do not actually run this command)
# DROP table customers;










