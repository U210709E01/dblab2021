-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema aircraftdb7
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `aircraftdb7` ;

-- -----------------------------------------------------
-- Schema aircraftdb7
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `aircraftdb7` DEFAULT CHARACTER SET utf8 ;
USE `aircraftdb7` ;

-- -----------------------------------------------------
-- Table `aircraftdb7`.`states`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aircraftdb7`.`states` (
  `state_id` INT NOT NULL,
  `state_name` VARCHAR(45) NULL,
  PRIMARY KEY (`state_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aircraftdb7`.`owners`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aircraftdb7`.`owners` (
  `owner_id` INT NOT NULL,
  `owner_name` VARCHAR(300) NULL,
  `owner_type` VARCHAR(30) NULL,
  `ownership_type` VARCHAR(20) NULL,
  `state_id` INT NOT NULL,
  PRIMARY KEY (`owner_id`, `state_id`),
  CONSTRAINT `fk_owners_states1`
    FOREIGN KEY (`state_id`)
    REFERENCES `aircraftdb7`.`states` (`state_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_owners_states1_idx` ON `aircraftdb7`.`owners` (`state_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `aircraftdb7`.`operators`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aircraftdb7`.`operators` (
  `operator_id` INT NOT NULL,
  `operator_name` VARCHAR(300) NULL,
  `operator_type` VARCHAR(20) NULL,
  `state_id` INT NOT NULL,
  PRIMARY KEY (`operator_id`, `state_id`),
  CONSTRAINT `fk_operators_states1`
    FOREIGN KEY (`state_id`)
    REFERENCES `aircraftdb7`.`states` (`state_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_operators_states1_idx` ON `aircraftdb7`.`operators` (`state_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `aircraftdb7`.`aircrafts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aircraftdb7`.`aircrafts` (
  `aircraft_id` INT NOT NULL,
  `model` VARCHAR(100) NULL,
  `manufacturer` VARCHAR(100) NULL,
  `serial_number` VARCHAR(100) NULL,
  `manufacture_year` INT NULL,
  `max_takeoff_weight` INT NULL,
  `min_crew_members_number` INT NULL,
  `seats` INT NULL,
  `max_passengers_number` INT NULL,
  `use_rights` VARCHAR(300) NULL,
  `owner_id` INT NOT NULL,
  `operator_id` INT NOT NULL,
  PRIMARY KEY (`aircraft_id`, `owner_id`, `operator_id`),
  CONSTRAINT `fk_aircrafts_owners1`
    FOREIGN KEY (`owner_id`)
    REFERENCES `aircraftdb7`.`owners` (`owner_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aircrafts_operators1`
    FOREIGN KEY (`operator_id`)
    REFERENCES `aircraftdb7`.`operators` (`operator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_aircrafts_owners1_idx` ON `aircraftdb7`.`aircrafts` (`owner_id` ASC) VISIBLE;

CREATE INDEX `fk_aircrafts_operators1_idx` ON `aircraftdb7`.`aircrafts` (`operator_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `aircraftdb7`.`inspections`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aircraftdb7`.`inspections` (
  `inspection_id` INT NOT NULL,
  `inspection_type` VARCHAR(15) NULL,
  PRIMARY KEY (`inspection_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aircraftdb7`.`aircraft_inspections`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aircraftdb7`.`aircraft_inspections` (
  `aircraft_id` INT NOT NULL,
  `inspection_id` INT NOT NULL,
  `inspection_status` VARCHAR(20) NULL,
  `inspection_date` DATETIME NULL,
  PRIMARY KEY (`aircraft_id`, `inspection_id`),
  CONSTRAINT `fk_aircrafts_has_inspections_aircrafts`
    FOREIGN KEY (`aircraft_id`)
    REFERENCES `aircraftdb7`.`aircrafts` (`aircraft_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aircrafts_has_inspections_inspections1`
    FOREIGN KEY (`inspection_id`)
    REFERENCES `aircraftdb7`.`inspections` (`inspection_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_aircrafts_has_inspections_inspections1_idx` ON `aircraftdb7`.`aircraft_inspections` (`inspection_id` ASC) VISIBLE;

CREATE INDEX `fk_aircrafts_has_inspections_aircrafts_idx` ON `aircraftdb7`.`aircraft_inspections` (`aircraft_id` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `aircraftdb7`.`properties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aircraftdb7`.`properties` (
  `property_id` INT NOT NULL,
  `property_name` VARCHAR(45) NULL,
  PRIMARY KEY (`property_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `aircraftdb7`.`aircrafts_properties_value`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `aircraftdb7`.`aircrafts_properties_value` (
  `aircraft_id` INT NOT NULL,
  `property_id` INT NOT NULL,
  `property_value` VARCHAR(45) NULL,
  PRIMARY KEY (`aircraft_id`, `property_id`),
  CONSTRAINT `fk_aircrafts_has_properties_aircrafts1`
    FOREIGN KEY (`aircraft_id`)
    REFERENCES `aircraftdb7`.`aircrafts` (`aircraft_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aircrafts_has_properties_properties1`
    FOREIGN KEY (`property_id`)
    REFERENCES `aircraftdb7`.`properties` (`property_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_aircrafts_has_properties_properties1_idx` ON `aircraftdb7`.`aircrafts_properties_value` (`property_id` ASC) VISIBLE;

CREATE INDEX `fk_aircrafts_has_properties_aircrafts1_idx` ON `aircraftdb7`.`aircrafts_properties_value` (`aircraft_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
