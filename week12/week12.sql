USE test;

EXPLAIN
SELECT * 
FROM proteins 
WHERE pid 
LIKE "5HT2C_HUMA%";

/* if data is full text based you may consider using hash */
CREATE UNIQUE INDEX idx1 USING BTREE ON proteins(pid);

ALTER TABLE proteins DROP INDEX idx1;


EXPLAIN
SELECT * 
FROM proteins 
WHERE accession = "Q9UBA6";

ALTER TABLE proteins ADD CONSTRAINT acc_pk PRIMARY KEY (accession);

EXPLAIN
SELECT * 
FROM proteins 
WHERE accession = "Q9UBA6";
















